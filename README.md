# NBA

## Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm start
```

#### Run your unit tests
```
npm run test:unit
```

#### Lints and fixes files
```
npm run lint
```

## General requirements

* ✅ Use this API https://www.balldontlie.io/
* ✅ User should see list of players
* ✅ After clicking on a player, user should be redirected to another view (dashboard) representing that player's statistics
Choose any data you want to represent on the dashboard (at least two series - each on a seperate block in a dashboard)
One of the series should be represented on a chart (for example number of games played within a season which you can find on https://www.balldontlie.io/api/v1/season_averages?season=2018&player_ids[]=1)
* ✅ Application should be responsive and work on desktop, tablet and mobile devices
* ✅ User should know when something is fetching (eg. spinner / loader)
* ✅ Project should run by using only two commands: npm install and npm start
* ✅ Please use Typescript in the project


## Tech stack
* Vue `3.0.0`
* Vuex `4.0.2`
* Vuetify `3.0.0-alpha.0`
* ApexCharts `3.27.3`
* Typescript `4.1.5`

## What could be improvement?
* Store is used only on Dashboard view. It should be also used on Player view.
* App should be run in Docker
* API url should be as global const
* Variables and scss files structures for styles
* Better typing, replace <any> with the correct type
* On Player view loader could be used for every single card (simultaneously card as placeholder)

## Screenshots
### Dashboard
![Dashboard](https://bitbucket.org/damianszkudlarek/nba-app/raw/05446c7725a6cc43169be949f07ffe9c447609db/public/screenshots/nba1.png)

### Loading Player
![Loading Player](https://bitbucket.org/damianszkudlarek/nba-app/raw/05446c7725a6cc43169be949f07ffe9c447609db/public/screenshots/nba2.png)

### Player
![Player](https://bitbucket.org/damianszkudlarek/nba-app/raw/05446c7725a6cc43169be949f07ffe9c447609db/public/screenshots/nba3.png)