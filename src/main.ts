import { createApp } from 'vue';
import VueApexCharts from 'vue3-apexcharts';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import router from './router/index';
import { store } from './store';

createApp(App)
  .use(vuetify)
  .use(router)
  .use(VueApexCharts)
  .use(store)
  .mount('#app');
