import { GetterTree } from 'vuex';
import { State } from './state';

// eslint-disable-next-line no-shadow
export enum GettersTypes {
  GET_PAGES = 'GET_PAGES',
  GET_PAGE_DATA = 'GET_PAGE_DATA',
  GET_CURRENT_PAGE = 'GET_CURRENT_PAGE',
  GET_TOTAL_PAGES = 'GET_TOTAL_PAGES',
}

export type Getters = {
  [GettersTypes.GET_PAGES](state: State): []
  [GettersTypes.GET_CURRENT_PAGE](state: State): number,
  [GettersTypes.GET_TOTAL_PAGES](state: State): number,
  [GettersTypes.GET_PAGE_DATA](state: State, payload: { pageNumber:number }): []
}

export const getters: GetterTree<State, State> & Getters = {
  [GettersTypes.GET_PAGES]: (state) => state.pages,
  [GettersTypes.GET_CURRENT_PAGE]: (state) => state.currentPage,
  [GettersTypes.GET_TOTAL_PAGES]: (state) => state.totalPages,
  [GettersTypes.GET_PAGE_DATA]: (state, { pageNumber }) => state.pages[pageNumber],
};
