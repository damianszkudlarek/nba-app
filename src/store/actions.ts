import { ActionTree, ActionContext } from 'vuex';
import axios, { AxiosResponse } from 'axios';
import { Players } from '@/interfaces/PlayersInterface';
import { getPagesResult } from '@/interfaces/GetPagesResult';
import camelcaseKeys from 'camelcase-keys';
import { State } from './state';
import { Mutations, MutationTypes } from './mutations';
import { getters, GettersTypes } from './getters';

// eslint-disable-next-line no-shadow
export enum ActionTypes {
  GET_PAGES = 'GET_PAGES',
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>
} & Omit<ActionContext<State, State>, 'commit'>

export interface Actions {
  [ActionTypes.GET_PAGES](
    { commit, state }: AugmentedActionContext,
    payload: {
      pageNumber: number
    }
  ): Promise<getPagesResult>
}

export const actions: ActionTree<State, State> & Actions = {

  async [ActionTypes.GET_PAGES]({ commit, state }, { pageNumber }) {
    if (!getters[GettersTypes.GET_PAGE_DATA](state, { pageNumber })) {
      await new Promise((resolve) => {
        axios.request<Players>({
          url: 'https://www.balldontlie.io/api/v1/players/',
          params: {
            per_page: 24,
            page: pageNumber,
          },
          transformResponse: [
            (data) => camelcaseKeys(JSON.parse(data), { deep: true }),
          ],
        }).then((response:AxiosResponse<Players>) => {
          const pageData = response.data.data;
          commit(MutationTypes.SET_PAGES, { pageData, pageNumber });
          commit(MutationTypes.SET_TOTAL_PAGES, response.data.meta.totalPages);
          resolve(getters[GettersTypes.GET_PAGE_DATA](state, { pageNumber }));
        });
      });
    }
    return {
      players: getters[GettersTypes.GET_PAGE_DATA](state, { pageNumber }),
      totalPages: getters[GettersTypes.GET_TOTAL_PAGES](state),
    };
  },
};
