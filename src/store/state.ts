export const state = {
  pages: <any>[],
  currentPage: 1,
  totalPages: 1,
};

export type State = typeof state;
