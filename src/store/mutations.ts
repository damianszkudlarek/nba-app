import { MutationTree } from 'vuex';
import { Player } from '@/interfaces/PlayerInterface';
import { State } from './state';

// eslint-disable-next-line no-shadow
export enum MutationTypes {
  SET_PAGES = 'SET_PAGES',
  SET_CURRENT_PAGE = 'SET_CURRENT_PAGE',
  SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'
}

export type Mutations<S = State> = {
  [MutationTypes.SET_PAGES](state: S, payload: { pageData: Array<Player>, pageNumber: number })
    : void
  [MutationTypes.SET_CURRENT_PAGE](state: S, currentPage: number): void
  [MutationTypes.SET_TOTAL_PAGES](state: S, totalPages: number): void
}

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_PAGES](state, { pageData, pageNumber }) {
    state.pages[pageNumber] = pageData;
  },
  [MutationTypes.SET_CURRENT_PAGE](state, currentPage) {
    state.currentPage = currentPage;
  },
  [MutationTypes.SET_TOTAL_PAGES](state, totalPages) {
    state.totalPages = totalPages;
  },
};
