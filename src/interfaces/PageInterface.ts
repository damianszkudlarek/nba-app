import { Player } from './PlayerInterface';

export interface Page {
  data: Array<Player>,
}
