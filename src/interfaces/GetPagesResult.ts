import { Player } from './PlayerInterface';

export interface getPagesResult {
  players: Array<Player>,
  totalPages: number
}
