import { Player } from '@/interfaces/PlayerInterface';
import { Meta } from '@/interfaces/MetaInterface';

export interface Players {
  data: Array<Player>,
  meta: Meta,
}
