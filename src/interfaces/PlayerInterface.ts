import { Team, defaultTeam } from '@/interfaces/TeamInterface';

export interface Player {
  id: number | null,
  firstName: string | null,
  lastName: string | null,
  position: string | null,
  heightFeet: string | null,
  heightInches: string | null,
  weightPounds: string | null,
  team: Team | null,
}

export const defaultPlayer: Player = {
  id: null,
  firstName: null,
  lastName: null,
  position: null,
  heightFeet: null,
  heightInches: null,
  weightPounds: null,
  team: defaultTeam,
};
