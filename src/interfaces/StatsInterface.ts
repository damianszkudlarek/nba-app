import { Meta } from '@/interfaces/MetaInterface';

export interface Stats {
  meta: Meta,
  data: Array<any>,
}

export const defaultStats: Stats = {
  meta: {
    currentPage: 0,
    nextPage: 0,
    perPage: 0,
    totalCount: 0,
    totalPages: 0,
  },
  data: [],
};
