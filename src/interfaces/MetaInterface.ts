export interface Meta {
  currentPage: number,
  nextPage: number,
  perPage: number,
  totalCount: number,
  totalPages: number,
}
