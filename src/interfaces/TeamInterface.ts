export interface Team {
  fullName: string,
  conference: string,
  division: string,
  city: string,
}

export const defaultTeam: Team = {
  fullName: '',
  conference: '',
  division: '',
  city: '',
};
