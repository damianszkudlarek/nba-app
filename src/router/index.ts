import { createWebHistory, createRouter } from 'vue-router';
import Player from '@/components/Player.vue';
import Dashboard from '@/components/Dashboard.vue';

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
  },
  {
    path: '/player/:id',
    name: 'player',
    component: Player,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
