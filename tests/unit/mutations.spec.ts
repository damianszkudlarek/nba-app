import { mutations, MutationTypes } from '@/store/mutations';
import { state } from '@/store/state';

// destructure assign `mutations`
// const { increment } = mutations;

describe('mutations', () => {
  it('Test mutation SET_TOTAL_PAGES', () => {
    expect(state.totalPages).toEqual(1);
    mutations[MutationTypes.SET_TOTAL_PAGES](state, 99);
    expect(state.totalPages).toEqual(99);
  });
  it('Test mutation SET_CURRENT_PAGE', () => {
    expect(state.currentPage).toEqual(1);
    mutations[MutationTypes.SET_CURRENT_PAGE](state, 2);
    expect(state.currentPage).toEqual(2);
  });
  it('Test mutation SET_PAGES', () => {
    expect(state.pages).toEqual([]);
    mutations[MutationTypes.SET_PAGES](state, { pageData: [1, 2], pageNumber: 3 });
    expect(state.pages[3]).toEqual([1, 2]);
  });
});
