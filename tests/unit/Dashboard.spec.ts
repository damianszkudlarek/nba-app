import { mount } from '@vue/test-utils';
import Dashboard from '@/components/Dashboard.vue';
import Vuex from 'vuex';

const getPagesFunction = jest.fn();
getPagesFunction.mockReturnValue({ players: [], totalPages: 1 });

const actions = {
  GET_PAGES: getPagesFunction,
};

const $store = new Vuex.Store({
  state: {},
  actions,
});

describe('Dashboard.vue', () => {
  it('Is rendering and displays a player', () => {
    const msg = 'Dashboard';
    const wrapper = mount(Dashboard, {
      data() {
        return {
          isLoading: false,
          players: [{ id: 1, first_name: 'John', last_name: 'Doe' }],
        };
      },
      global: {
        mocks: {
          $store,
        },
      },
    });
    expect(wrapper.text()).toMatch(msg);
    expect(wrapper.find('.nbaDashboard__playerName').exists()).toBe(true);
    wrapper.setData({
      pagination: {
        current: 1,
        total: 99,
      },
    });
    expect(wrapper.vm.pagination.total).toBe(99);
  });
});
